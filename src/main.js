import Vue from "vue"
import App from "./App.vue"

import upperFirst from "lodash/upperFirst"
import camelCase from "lodash/camelCase"
import router from "./router"
import store from "./store"
import BootstrapVue from "bootstrap-vue"
import UUID from "vue-uuid"
import "bootstrap/dist/css/bootstrap.css"
import "bootstrap-vue/dist/bootstrap-vue.css"

const requireComponent = require.context(
  "./components",
  false,
  /Base[A-Z]\w+\.(vue|js)$/
)

requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName)

  const componentName = upperFirst(
    camelCase(fileName.replace(/^\.\/(.*)\.\w+$/, "$1"))
  )

  Vue.component(componentName, componentConfig.default || componentConfig)
})

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(UUID)

Vue.filter("displayedValue", function(value) {
  return new Intl.NumberFormat().format(value)
})

Vue.filter("displayedDateTime", function(value) {
  return `${value.getFullYear()}/${value.getMonth() + 1}/${value.getDate()} at 
    ${value.getHours()}:${value.getMinutes()}:${value.getSeconds()}`
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app")
