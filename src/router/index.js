import Vue from "vue"
import VueRouter from "vue-router"
import Home from "../views/Home.vue"
import Converter from "../views/Converter.vue"

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/converter/:currency",
    name: "converter",
    component: Converter,
    props: true
  }
]

const router = new VueRouter({
  mode: "history",
  routes
})

export default router
