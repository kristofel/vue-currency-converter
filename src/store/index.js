import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    purchases: []
  },
  getters: {
    countPurchases: state => {
      return state.purchases.length
    }
  },
  mutations: {
    ADD_PURCHASE: (state, purchase) => {
      state.purchases.push(purchase)
    }
  },
  actions: {
    addPurchase: (context, purchase) => {
      context.commit("ADD_PURCHASE", purchase)
    }
  },
  modules: {}
})
