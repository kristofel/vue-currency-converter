import axios from "axios"

const apiClient = axios.create({
  baseURL: "https://api.exchangeratesapi.io",
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
})

export default {
  getCurrencyData(currencyName) {
    return apiClient.get("/latest?base=" + currencyName)
  }
}
