export default {
  availableConversionCurrencies: [
    { value: "CZK", text: "CZK - Czech Koruna" },
    { value: "EUR", text: "EUR - Euro" },
    { value: "GBP", text: "GBP - British Pound" },
    { value: "PLN", text: "PLN - Polish Zloty" },
    { value: "USD", text: "USD - US Dollar" }
  ]
}
