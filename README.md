# vue-currency-converter

Vue Currency Converter is a project made by Krzysztof Kobus as a recrutation sample for Vue code.

Exchange rates are fetched from a free service for current and historical foreign exchange rates
published by the European Central Bank.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
